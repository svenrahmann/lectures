# Lectures

This repository contains Jupyter notebooks for lectures by Sven Rahmann.

(c) 2018-present by Sven Rahmann.

Contact the author if you would like to re-use the material.

# Environment for the notebooks

```bash
conda create --name lectures  python=3.6 jupyter seaborn numpy pandas numba scipy snakemake jupyter_contrib_nbextensions
```
