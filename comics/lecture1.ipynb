{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "![Logo](files/img/logo.png)\n",
    "\n",
    "# Computational Omics Lecture 1\n",
    "\n",
    "Winter 2018/19, Computer Science, TU Dortmund\n",
    "\n",
    "**Instructor:**<br/>\n",
    "Prof. Sven Rahmann<br/>\n",
    "Genome Informatics<br/>\n",
    "University of Duisburg-Essen\n",
    "\n",
    "**Welcome!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Teaching Strategy\n",
    "\n",
    "* Learning by doing\n",
    "* Theory background taught in class\n",
    "* Reading assignments at home\n",
    "* Questions & answers and discussions during class\n",
    "* Small practical projects every week\n",
    "* Larger final project\n",
    "* Final exam involves the final project"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## What you sould bring to this course\n",
    "\n",
    "* Don’t be afraid of biology and chemistry\n",
    "* Good algorithms and data structures background\n",
    "* Basic probability and statistics\n",
    "* Basic numerical linear algebra\n",
    "* Good programming skills\n",
    "* Know how to plot data\n",
    "\n",
    "* **Note**: Weaknesses in the above areas will mean that you need to invest more time in the course."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Recommended programming language\n",
    "\n",
    "* Python 3 (+ numpy/scipy + pandas + matplotlib + seaborn + ...)\n",
    "* Install and manage packages with [conda](https://conda.io/miniconda.html \"Miniconda\"); use the Python 3.x 64-bit installer for your OS.\n",
    "* You can also use R, Julia, Rust, or others, but I cannot offer support."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# What is the Course About?\n",
    "## A Little Biology and Technology"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "\n",
    "| Cells | Biomolecules | Networks |\n",
    "|:----:|:----:|:----:|\n",
    "| building blocks of life | DNA, RNA, proteins, organic compounds | DNA/RNA, protein/DNA, protein/protein, protein/ligand, ... |\n",
    "| <img src=\"files/img-01/cell.png\" alt=\"cell\" width=\"500px\"/> | <img src=\"files/img-01/rnadna.png\" alt=\"rna/dna\" width=\"500px\"/> <img src=\"files/img-01/protein.png\" alt=\"protein\" width=\"500px\"/> | <img src=\"files/img-01/network1.png\" alt=\"network\" width=\"500px\"/> <img src=\"files/img-01/network2.png\" alt=\"network\" width=\"500px\"/>\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "\n",
    "| High-throughput analytics | Data and Data Analysis |\n",
    "|:----:|:----:|\n",
    "| DNA/RNA sequencing, mass spectrometry, cell imaging | Algorithms, data structures, probability & statistics, numerical mathematics, workflow management |\n",
    "| <img src=\"files/img-01/sequencing.png\" alt=\"sequencing\" width=\"100px\"/>  <img src=\"files/img-01/mass-spec.png\" alt=\"mass-spectrometry\" width=\"200px\"/>  <img src=\"files/img-01/imaging.png\" alt=\"imagong\" width=\"100px\"/> | <img src=\"files/img-01/data.png\" alt=\"network\" width=\"300px\"/> |\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## DNA and RNA Structure\n",
    "\n",
    "* RNA (ACGU, single-stranded): “copies” of parts of DNA (genes); regulatory functions\n",
    "* DNA (ACGT, double-stranded): encodes our genetic material\n",
    "\n",
    "<img src=\"files/img-01/rnadna.png\" alt=\"RNA and DNA structure\" width=\"600px\"/>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Definitions for Computer Scientists\n",
    "\n",
    "* A **chromosome** is a (long) string over the DNA alphabet,\n",
    "modulo the equivalence relation of reverse complementarity.\n",
    "  Example: `AAAAGG` == `CCTTTT`\n",
    "\n",
    "* A **gene** is an interval on a chromosome (with a particular structure and purpose).\n",
    "\n",
    "* The (typical) **genome** of a species consists of the DNA sequence of all chromosomes (human: 22+X+Y+M)\n",
    "\n",
    "\n",
    "### Exercises\n",
    "\n",
    "1. How many (single-stranded) RNA sequences are there of length $n$ ?\n",
    "2. How many DNA sequences are there of length $n$ ?\n",
    "(This is harder than it looks! Solve $n = 1, 2, 3, 4$ first.)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## DNA Sequencing Technologies\n",
    "\n",
    "We cannot sequence a whole chromosome at a time.<br/>\n",
    "E.g. chromosome 1 of the human genome has 250 Mbp (million base pairs).\n",
    "\n",
    "Some technologies on the market\n",
    "* Illumina: short reads (2x100 bp paired-end), low error rates\n",
    "* Pacific Biosciences: longer reads, higher error rates\n",
    "* Oxford Nanopore: possibly very long reads, error rates 10%-15%\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## File Formats for Sequence Data\n",
    "\n",
    "**FASTA**: genomic DNA (chromosome, genes, ...):<br/>\n",
    "1 header + $n$ sequence lines per entry\n",
    "\n",
    "```text\n",
    ">header\n",
    "sequencesequencesequencesequencese\n",
    "quencesequencesequencesequencesequ\n",
    ">anotherheader\n",
    "anothersequenceanothersequenceanot\n",
    "```\n",
    "\n",
    "**FASTQ**: (sequenced) short DNA fragments with qualities:<br/>\n",
    "4 lines per entry (header, sequence, plus, qualities)\n",
    "```text\n",
    "@header\n",
    "sequencesequence\n",
    "+\n",
    "QUALITYQUALITYQU\n",
    "```\n",
    "\n",
    "Typically found in gzipped format (extensions `.fa.gz`, `.fq.gz`).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Base Quality values (Phred scale)\n",
    "\n",
    "\n",
    "DNA sequencers produce errors for many reasons:\n",
    "\n",
    "* errors during PCR amplification\n",
    "* low signal/noise ratio at a specific spot at a specific time\n",
    "* de-phasing\n",
    "\n",
    "The sequencers estimate an error probability $p$ for each base call.\n",
    "They do not know the real error probability, it is just an estimate!\n",
    "\n",
    "**Definition:**\n",
    "\n",
    "* Phred quality $Q = -10 \\log_{10}(p)$ \n",
    "* Example: $p = 1/1000$ (99.9% correct) means $Q = 30$\n",
    "* The scale for $Q$ is 0 .. 63.\n",
    "\n",
    "We usually consider only sequence parts where consistently $Q > 20$ (>99% correct).\n",
    "\n",
    "**Encoding:**\n",
    "\n",
    "* $Q$ is often ASCII-encoded by offset 33 in FASTQ: chr($Q$+33)\n",
    "* Different encodings exist!\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Coverage\n",
    "\n",
    "* $L$: length of the region of interest (e.g., genome size)\n",
    "* $F$: length of a sequenced DNA fragment (e.g., 2x100 bp)\n",
    "* $N$: number of sequenced DNA fragments\n",
    "* Then the (average) coverage is  $C := FN / L$\n",
    "\n",
    "For genomic sequencing, $C$ > 30 is desired\n",
    "(for error correction, reliable variant calling, etc.).\n",
    "\n",
    "**Exercise:**\n",
    "How many Illumina reads (2x100 bp) are required for 30x coverage of the human genome (3.3 Gbp)?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Coverage Distribution\n",
    "\n",
    "With average coverage $C$,\n",
    "what is the probability to observe coverage $k$ at a given genomic position (assuming random starting points) ?\n",
    "\n",
    "Coverage has a Poisson($C$) distribution (Lander-Waterman model).\n",
    "\n",
    "$$ \\text{Prob}(\\text{Coverage} = k) =  C^k \\cdot \\exp(-C) \\,/\\, k ! $$\n",
    "\n",
    "**Derivation:**\n",
    "Poisson approximation to the Binomial distribution\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Assignments\n",
    "\n",
    "* Watch some of the sequencing technology videos listed among the additional material.\n",
    "* In particular, watch the overview lecture given by Dr. Elaine Mardis (2014).\n",
    "* Be sure to understand the Poisson approximation to the Binomial distribution.\n",
    "\n",
    "**Note:** \"Watch\" means \"watch, take notes, go back and review, understand, prepare questions\".\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
